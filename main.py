# This is a sample Python script.
from scripts.core.calCircle import Circle
from scripts.core.calRectangle import Rectangle
from scripts.core.calSquare import Square

print("Rectangle: Enter length and breadth:")
l, b = map(int, input().split())
rectangleObject = Rectangle(l, b)
print("Area:", rectangleObject.area(), "Perimeter:", rectangleObject.perimeter())
print("Square: Enter side:")
l = int(input())
squareObject = Square(l)
print("Area:", squareObject.area(), "Perimeter:", squareObject.perimeter())
print("Circle: Enter radius:")
r = int(input())
circleObject = Circle(r)
print("Area:", circleObject.area(), "Perimeter:", circleObject.perimeter())

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
